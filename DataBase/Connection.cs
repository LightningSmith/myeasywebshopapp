﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AddProduct.GUI;
using AddProduct.Logic.Abdurrahman;
using MySql.Data.MySqlClient;

namespace AddProduct.DataBase
{
    class Connection
    {
        private string quote = "\"";
        private List<string> info = new List<string>();
        private string connection = @"Server=db.myeasywebshop.com; Database=webshop; Uid=Database; Pwd=Myeasywebshop123!; SslMode=Required";
       
        public MySqlConnection Connections()
        {
            MySqlConnection con = new MySqlConnection(connection);
            con.Open();

            return con;
        }

        public List<string> GetShopData(MySqlConnection con, string shopName)
        {
            string sql = "SELECT * FROM shop WHERE shopname = " + quote + shopName + quote;
            MySqlCommand cmd = new MySqlCommand(sql, con);

            MySqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                info.Add((rdr["id"].ToString()));
                info.Add((rdr["shopname"].ToString()));
                info.Add((rdr["slogan"].ToString()));
                info.Add((rdr["email"].ToString()));
                info.Add((rdr["phonenumber"].ToString()));
                info.Add((rdr["address"].ToString()));
                info.Add((rdr["productinfo"].ToString()));
            }
            con.Close();

            return info;
        }

        public void UpdateShopData(MySqlConnection con, string shopId, string shopName, string slogan, string email, string phonenumber, string address, string companyInfo)
        {
            string sql1 = "UPDATE shop SET shopname = @shopName, slogan = @slogan, email = @email, phonenumber = @phonenumber, address = @address, productinfo = @productinfo WHERE id = @ID;";
            MySqlCommand cmd1 = new MySqlCommand(sql1, con);
            
            cmd1.Parameters.AddWithValue("@shopName", shopName);
            cmd1.Parameters.AddWithValue("@slogan", slogan);
            cmd1.Parameters.AddWithValue("@email", email);
            cmd1.Parameters.AddWithValue("@phonenumber", phonenumber);
            cmd1.Parameters.AddWithValue("@address", address);
            cmd1.Parameters.AddWithValue("@productinfo", companyInfo);
            cmd1.Parameters.AddWithValue("@ID", shopId);
            
            cmd1.Prepare();
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Data send");
            con.Close();
            
        }

        public void AddProduct(MySqlConnection con, string productName, string productPrice, string productDescription)
        {

                string sql2 = "INSERT INTO product_import(productname, price, description) VALUES(@productname, @price, @description)";
                MySqlCommand cmd2 = new MySqlCommand(sql2, con);

                cmd2.Parameters.AddWithValue("@productname", productName);
                cmd2.Parameters.AddWithValue("@price", productPrice);
                cmd2.Parameters.AddWithValue("@description", productDescription);

                cmd2.Prepare();
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Data send");
  
            con.Close();
        }
    }
}