﻿
using AddProduct.GUI;
using AddProduct.Logic.Abdurrahman;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddProduct
{
    public partial class Cliënt : Form
    {
        //private string userName;
        //private string passWord;

        public Cliënt()
        {
            InitializeComponent();
        }

        private void SignIn_Click(object sender, EventArgs e)
        {
            Dashboard dashboard = new Dashboard();

            dashboard.Show();

            this.Hide();
        }


        private void SignUp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CliëntLogic cliënt = new CliëntLogic();
            try
            {
                cliënt.VisitLink(SignUp);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open link that was clicked.");
            }
        }
          
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
