﻿using AddProduct.DataBase;
using AddProduct.Logic;
using AddProduct.Logic.Kris;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AddProduct.GUI
{
    public partial class Products : Form
    {
        public Products()
        {
            InitializeComponent();
        }

        private void Product_Load(object sender, EventArgs e)
        {
          
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if(txtUrl.Text != "")
            {
                ProductLogic product = new ProductLogic(txtUrl.Text, cmbSite.Text); 
            
                txtProductName.Text = product.Name;
                txtProductPrice.Text = product.Price;
                txtProductDescription.Text = product.Description;
                product.CloseDriver();  
            }

            else
            {
                ProductLogic product = new ProductLogic();

                txtProductName.Text = product.Name;
                txtProductPrice.Text = product.Price;
                txtProductDescription.Text = product.Description;
                product.CloseDriver();
            }
                
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            MySqlConnection con =  connection.Connections();

            connection.AddProduct(con, txtProductName.Text, txtProductPrice.Text, txtProductDescription.Text);
        }
    }
}
