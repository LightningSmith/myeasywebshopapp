﻿using AddProduct.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using AddProduct.Logic.Abdurrahman;

namespace AddProduct.GUI
{
    public partial class Webshop : Form
    {

        public Webshop()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> webshopData = new List<string>();
            WebshopLogic webshop = new WebshopLogic();
            
            if (textBox1.Text == "")
            {
                MessageBox.Show("no given shop name");
            }
            else
            {
                webshopData = webshop.GetData(textBox1);
                webshopid.Text = webshopData[0];
                textBox1.Text = webshopData[1];
                textBox2.Text = webshopData[2];
                textBox3.Text = webshopData[3];
                textBox4.Text = webshopData[4];
                textBox5.Text = webshopData[5];
                textBox6.Text = webshopData[6];
            }         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WebshopLogic webshop = new WebshopLogic();

            string shopId = webshopid.Text;
            string shopName = textBox1.Text;
            string slogan = textBox2.Text;
            string email = textBox3.Text;
            string phonenumber = textBox4.Text;
            string address = textBox5.Text;
            string companyInfo = textBox6.Text;

            webshop.SendData(shopId, shopName, slogan, email, phonenumber, address, companyInfo);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
