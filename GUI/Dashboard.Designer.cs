﻿namespace AddProduct.GUI
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webshop = new System.Windows.Forms.Button();
            this.addProduct = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webshop
            // 
            this.webshop.Location = new System.Drawing.Point(178, 349);
            this.webshop.Name = "webshop";
            this.webshop.Size = new System.Drawing.Size(270, 98);
            this.webshop.TabIndex = 0;
            this.webshop.Text = "webshop";
            this.webshop.UseVisualStyleBackColor = true;
            this.webshop.Click += new System.EventHandler(this.webshop_Click);
            // 
            // addProduct
            // 
            this.addProduct.Location = new System.Drawing.Point(178, 600);
            this.addProduct.Name = "addProduct";
            this.addProduct.Size = new System.Drawing.Size(270, 98);
            this.addProduct.TabIndex = 1;
            this.addProduct.Text = "add product";
            this.addProduct.UseVisualStyleBackColor = true;
            this.addProduct.Click += new System.EventHandler(this.addProduct_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 1061);
            this.Controls.Add(this.addProduct);
            this.Controls.Add(this.webshop);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button webshop;
        private System.Windows.Forms.Button addProduct;
    }
}