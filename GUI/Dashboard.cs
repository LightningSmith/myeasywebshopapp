﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddProduct.GUI
{
    public partial class Dashboard : Form
    {
        public static Form PreviousPage;
        public Dashboard()
        {
            InitializeComponent();
        }

        private void webshop_Click(object sender, EventArgs e)
        {
            Webshop webshop = new Webshop();

            webshop.Show();

            this.Hide();
        }

        private void addProduct_Click(object sender, EventArgs e)
        {
            Products products = new Products();

            products.Show();

            this.Hide();
        }
    }
}
