﻿using AddProduct.GUI;
using AddProduct.Logic.Kris;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;

namespace AddProduct.Logic
{
    class ProductLogic
    {
        SeleniumLogic ProductDriver = new SeleniumLogic();

        //fields
        private string url;
        private string site; 

        //properties
        public string Name
        {
            get { return ProductDriver.GatherName(site);} 
        }

        public string Price
        {
            get { return ProductDriver.GatherPrice(site); }  
        }

        public string Description
        {
            get { return ProductDriver.GatherDescription(site);  }
        }

        //methods
        public void CloseDriver()
        {
            ProductDriver.DriverClose(); 
        }

        //constructor 
        public ProductLogic(string url, string site)
        {
            this.url = url;
            this.site = site;

            ProductDriver.GoToUrl(url); 
        }

        public ProductLogic()
        {
            this.site = "Amazon";
            this.url = "https://www.amazon.com/Last-Us-Part-II-PlayStation-4/dp/B07DJRFSDF/ref=sr_1_1?dchild=1&fst=as%3Aoff&pf_rd_i=16225016011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=03b28c2c-71e9-4947-aa06-f8b5dc8bf880&pf_rd_r=HGH585ZGJ5CNSBVVWEPW&pf_rd_s=merchandised-search-3&pf_rd_t=101&qid=1510597020&refinements=p_89%3APlaystation&rnid=2528832011&s=videogames-intl-ship&sr=1-1";

            ProductDriver.GoToUrl(this.url); 
        }
    }
}
