﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using AddProduct.Logic.Kris;

namespace AddProduct.Logic
{
    class SeleniumLogic
    {
        IWebDriver driver = new ChromeDriver();
        
        //methods
        public string GatherPrice(string site)
        { 
            if(site == "Amazon" )
            {
                try
                {
                    return driver.FindElement(By.Id("priceblock_dealprice")).Text;
                }

                catch
                {
                    return driver.FindElement(By.Id("priceblock_ourprice")).Text;
                }
            }

            else
            {
                try
                {
                    driver.FindElement(By.ClassName("next-dialog-close")).Click(); 
                    return driver.FindElement(By.ClassName("product-price-value")).Text;
                }

                catch
                {
                    return driver.FindElement(By.ClassName("product-price-value")).Text; 
                }
            }
            
        }

        public string GatherName(string site)
        {
            if (site == "Amazon")
            {
               
                return driver.FindElement(By.Id("productTitle")).Text; 
            }

            else
            {
                return driver.FindElement(By.ClassName("product-title-text")).Text;
            }
            
        }

        public string GatherDescription(string site)
        {   
            if (site == "Amazon")
            {
                return driver.FindElement(By.Id("feature-bullets")).Text; 
            }
            
            else
            {
                try
                {
                    return driver.FindElement(By.Id("product-description")).Text;
                }
                 
                catch
                {
                    return "Could not load description"; 
                }
            }
           
        }

        public void GoToUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
            Thread.Sleep(2000); 
        }

        public void DriverClose()
        {
            driver.Close();
            driver.Quit();      
        }
    }
}
