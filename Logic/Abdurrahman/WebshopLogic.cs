﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AddProduct.DataBase;
using MySql.Data.MySqlClient;

namespace AddProduct.Logic.Abdurrahman
{
    class WebshopLogic
    {
        public List<string> GetData(TextBox shopName)
        {
            List<string> shopData = new List<string>();
            Connection connection = new Connection();
            MySqlConnection con = connection.Connections();

            shopData = connection.GetShopData(con, shopName.Text);

            return shopData;
        }

        public void SendData(string shopId, string shopName, string slogan, string email, string phonenumber, string address, string companyInfo)
        {
            Connection connection = new Connection();
            MySqlConnection con = connection.Connections();

            connection.UpdateShopData(con, shopId, shopName, slogan, email, phonenumber, address, companyInfo);
        }
    }
}
